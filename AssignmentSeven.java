import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by MA388504 on 1/2/2018.
 */
public class AssignmentSeven {
    public static void main(String[] args) {
        List<String> strings = Arrays.asList("abcfsd", "", "bsdfc", "efg", "abcd","", "jkl","fsd","fasdfas","","fasfas");
        System.out.println("List: " +strings);


        long lengthFiveCount = strings.stream().filter(string -> string.length() > 5).count();
        System.out.println("Strings of length >5: " + lengthFiveCount);

        long emptyCount = strings.stream().filter(string->string.isEmpty()).count();
        System.out.println("Empty Strings: " + emptyCount);

        List<String>filtered = strings.stream().filter(string ->!string.isEmpty()).collect(Collectors.toList());
        System.out.println("Filtered List: " + filtered);

        List<String>filteredEmpty = strings.stream().filter(string ->string.isEmpty()).collect(Collectors.toList());
        System.out.println("Filtered List of Empty Strings: " + filteredEmpty);

    }
}
