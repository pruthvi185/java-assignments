
/**
 * Created by MA388504 on 1/2/2018.
 */
public class AssignmentTwo {
}
interface Piano{
    default void play(){
        System.out.println("play() method inside Piano Interface");
    }
}
interface Guitar{
    default void play(){
        System.out.println("play() method inside Guitar Interface");
    }
}
class InstrumentMethodOne implements Piano,Guitar{

    public void play(){
        System.out.print("play() method inside Instrument class");
    }
}
class InstrumentMethodTwo implements Piano,Guitar{

    public void play(){
        Piano.super.play();
    }
}